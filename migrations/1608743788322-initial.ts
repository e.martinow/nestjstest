import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1608743788322 implements MigrationInterface {
    name = 'initial1608743788322'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            CREATE TABLE "product" (
                "id" SERIAL NOT NULL,
                "name" character varying NOT NULL,
                "price" integer NOT NULL,
                "description" character varying NOT NULL,
                "photo" character varying,
                "ownerId" integer,
                CONSTRAINT "PK_bebc9158e480b949565b4dc7a82" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "user" (
                "id" SERIAL NOT NULL,
                "username" character varying NOT NULL,
                "description" character varying,
                "photo" character varying,
                "password" character varying NOT NULL,
                CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            CREATE TABLE "cart" (
                "id" SERIAL NOT NULL,
                "userId" integer,
                CONSTRAINT "PK_c524ec48751b9b5bcfbf6e59be7" PRIMARY KEY ("id")
            )
        `);
        await queryRunner.query(`
            CREATE UNIQUE INDEX "IDX_756f53ab9466eb52a52619ee01" ON "cart" ("userId")
        `);
        await queryRunner.query(`
            CREATE TABLE "cart_products_product" (
                "cartId" integer NOT NULL,
                "productId" integer NOT NULL,
                CONSTRAINT "PK_785ab9c1dbede19ef42bf12280b" PRIMARY KEY ("cartId", "productId")
            )
        `);
        await queryRunner.query(`
            CREATE INDEX "IDX_e6ce39be5d354954a88ded1eba" ON "cart_products_product" ("cartId")
        `);
        await queryRunner.query(`
            CREATE INDEX "IDX_0fc996e42b6330c97f8cffbddf" ON "cart_products_product" ("productId")
        `);
        await queryRunner.query(`
            ALTER TABLE "product" DROP COLUMN "ownerId"
        `);
        await queryRunner.query(`
            ALTER TABLE "product"
            ADD "ownerId" integer
        `);
        await queryRunner.query(`
            ALTER TABLE "product"
            ALTER COLUMN "description" DROP NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "product"
            ADD CONSTRAINT "FK_cbb5d890de1519efa20c42bcd52" FOREIGN KEY ("ownerId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "cart"
            ADD CONSTRAINT "FK_756f53ab9466eb52a52619ee019" FOREIGN KEY ("userId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "cart_products_product"
            ADD CONSTRAINT "FK_e6ce39be5d354954a88ded1ebac" FOREIGN KEY ("cartId") REFERENCES "cart"("id") ON DELETE CASCADE ON UPDATE NO ACTION
        `);
        await queryRunner.query(`
            ALTER TABLE "cart_products_product"
            ADD CONSTRAINT "FK_0fc996e42b6330c97f8cffbddfa" FOREIGN KEY ("productId") REFERENCES "product"("id") ON DELETE CASCADE ON UPDATE NO ACTION
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "cart_products_product" DROP CONSTRAINT "FK_0fc996e42b6330c97f8cffbddfa"
        `);
        await queryRunner.query(`
            ALTER TABLE "cart_products_product" DROP CONSTRAINT "FK_e6ce39be5d354954a88ded1ebac"
        `);
        await queryRunner.query(`
            ALTER TABLE "cart" DROP CONSTRAINT "FK_756f53ab9466eb52a52619ee019"
        `);
        await queryRunner.query(`
            ALTER TABLE "product" DROP CONSTRAINT "FK_cbb5d890de1519efa20c42bcd52"
        `);
        await queryRunner.query(`
            ALTER TABLE "product"
            ALTER COLUMN "description"
            SET NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "product" DROP COLUMN "ownerId"
        `);
        await queryRunner.query(`
            ALTER TABLE "product"
            ADD "ownerId" integer
        `);
        await queryRunner.query(`
            DROP INDEX "IDX_0fc996e42b6330c97f8cffbddf"
        `);
        await queryRunner.query(`
            DROP INDEX "IDX_e6ce39be5d354954a88ded1eba"
        `);
        await queryRunner.query(`
            DROP TABLE "cart_products_product"
        `);
        await queryRunner.query(`
            DROP INDEX "IDX_756f53ab9466eb52a52619ee01"
        `);
        await queryRunner.query(`
            DROP TABLE "cart"
        `);
        await queryRunner.query(`
            DROP TABLE "user"
        `);
        await queryRunner.query(`
            DROP TABLE "product"
        `);
    }

}
