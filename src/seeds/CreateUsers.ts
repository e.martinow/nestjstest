import { Factory, Seeder } from 'typeorm-seeding';
import { Connection } from 'typeorm';
import { User } from '../users/schemas/users.entity';
import { HttpException } from '@nestjs/common';

export default class CreateUsers implements Seeder {
  public async run(factory: Factory, connection: Connection): Promise<any> {
    const bcrypt = await require('bcrypt');
    const salt = await bcrypt.genSaltSync(10);
    await connection
      .createQueryBuilder()
      .insert()
      .into(User)
      .values([
        {
          username: 'Admin',
          description: 'ban',
          password: bcrypt.hashSync('test', salt),
        },
        {
          username: 'User',
          description: 'naxyi',
          password: bcrypt.hashSync('test', salt),
        },
      ])
      .execute();
  }
}
