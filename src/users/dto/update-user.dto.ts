export class UpdateUserDto {
  readonly name: string;
  readonly description: string;
}
