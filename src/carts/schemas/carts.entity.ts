import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
  Index,
} from 'typeorm';
import { User } from '../../users/schemas/users.entity';
import { Product } from '../../products/schemas/products.entity';

@Entity()
export class Cart {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToMany(() => Product, {
    cascade: ['insert', 'update'],
  })
  @JoinTable()
  products: Product[];

  @ManyToOne(() => User, (user) => user.id, {
    cascade: ['insert', 'update'],
  })
  @Index({ unique: true })
  user: User;
}
