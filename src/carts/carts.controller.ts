import {
  Controller,
  Get,
  Delete,
  Put,
  Param,
  Request,
  UseGuards,
  Body,
} from '@nestjs/common';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { CartsService } from './carts.service';
import { UpdateProductDto } from '../products/dto/update-product.dto';
import { AddProductDto } from './dto/add-product.dto';

@Controller('cart')
export class CartsController {
  constructor(private readonly cartsService: CartsService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  getMy(@Request() req) {
    return this.cartsService.getMy(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  remove(@Request() req) {
    return this.cartsService.remove(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Put('add')
  addProduct(@Request() req, @Body() addProductDto: AddProductDto) {
    return this.cartsService.addProduct(req.user, addProductDto);
  }

  @UseGuards(JwtAuthGuard)
  @Put('remove')
  removeProduct(@Request() req, @Body() addProductDto: AddProductDto) {
    return this.cartsService.removeProduct(req.user, addProductDto);
  }
}
