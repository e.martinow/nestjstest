import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cart } from './schemas/carts.entity';
import { AddProductDto } from './dto/add-product.dto';
import { Product } from '../products/schemas/products.entity';

@Injectable()
export class CartsService {
  constructor(
    @InjectRepository(Cart)
    private cartsRepository: Repository<Cart>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  getMy(user) {
    return this.cartsRepository.findOne({
      where: { user: user },
      relations: ['products'],
    });
  }

  remove(user) {
    return this.cartsRepository.delete({ user: user }).catch();
  }

  async addProduct(user, addProductDto: AddProductDto) {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const self = this;
    const cart = await this.cartsRepository
      .findOneOrFail({
        where: { user: user },
        relations: ['products'],
      })
      .catch(function (e) {
        if (e.name === 'EntityNotFound') {
          const hui = new Cart();
          hui.user = user;
          hui.products = [];
          return self.cartsRepository.save(hui);
        } else {
          throw new HttpException(e.name, HttpStatus.INTERNAL_SERVER_ERROR);
        }
      });
    const product = await this.productsRepository
      .findOneOrFail(addProductDto.productId)
      .catch(function (e) {
        throw new HttpException(e.name, HttpStatus.INTERNAL_SERVER_ERROR);
      });
    cart.products.push(product);
    return await this.cartsRepository.save(cart);
  }

  async removeProduct(user, addProductDto: AddProductDto) {
    const cart = await this.cartsRepository
      .findOneOrFail({
        where: { user: user },
        relations: ['products'],
      })
      .catch(function (e) {
        throw new HttpException(e.name, HttpStatus.INTERNAL_SERVER_ERROR);
      });

    cart.products = cart.products.filter((product) => {
      product.id != addProductDto.productId;
    });

    return await this.cartsRepository.save(cart);
  }
}
