import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CartsController } from './carts.controller';
import { Cart } from './schemas/carts.entity';
import { CartsService } from './carts.service';
import { ProductsService } from '../products/products.service';
import { Product } from '../products/schemas/products.entity';

@Module({
  controllers: [CartsController],
  providers: [CartsService, ProductsService],
  imports: [TypeOrmModule.forFeature([Cart, Product])],
})
export class CartsModule {}
