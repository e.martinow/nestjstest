import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './schemas/products.entity';
import { UpdateProductDto } from './dto/update-product.dto';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  getAll() {
    return this.productsRepository.find();
  }

  getOne(id: number) {
    return this.productsRepository
      .findOneOrFail(id, {
        relations: ['owner'],
      })
      .catch(function () {
        throw new HttpException('Product not found', HttpStatus.NOT_FOUND);
      });
  }

  async create(user, createProductDto: CreateProductDto): Promise<Product> {
    const product = new Product();
    product.owner = user;
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    product.description = createProductDto.description ?? null;
    return await this.productsRepository.save(product);
  }

  async update(user, id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRepository
      .findOneOrFail(id, {
        relations: ['owner'],
      })
      .catch(function () {
        throw new HttpException('Product not found', HttpStatus.NOT_FOUND);
      });

    if (product.owner.id !== user.id) {
      throw new HttpException(
        'You can edit only your products',
        HttpStatus.FORBIDDEN,
      );
    }
    return await this.productsRepository.update(product, updateProductDto);
  }

  async remove(user, id: number) {
    const product = await this.productsRepository
      .findOneOrFail(id)
      .catch(function () {
        throw new HttpException('Product not found', HttpStatus.NOT_FOUND);
      });
    if (product.owner.id !== user.id) {
      throw new HttpException(
        'You can delete only your products',
        HttpStatus.FORBIDDEN,
      );
    }
    return await this.productsRepository.delete(id);
  }
}
