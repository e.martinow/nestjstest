import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { User } from '../../users/schemas/users.entity';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  description: string;

  @Column({ nullable: true })
  photo: string;

  @ManyToOne(() => User, (owner) => owner.products, {
    cascade: ['insert', 'update'],
  })
  @JoinColumn()
  owner: User;
}
